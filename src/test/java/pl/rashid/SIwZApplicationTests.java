package pl.rashid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.rashid.siwz.server.SIwZApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SIwZApplication.class)
public class SIwZApplicationTests {

	@Test
	public void contextLoads() {
	}

}
