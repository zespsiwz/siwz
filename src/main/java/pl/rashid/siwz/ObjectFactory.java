//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.07.02 at 06:28:21 PM CEST 
//


package pl.rashid.siwz;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.rashid.siwz package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.rashid.siwz
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllTemplatesRequest }
     * 
     */
    public GetAllTemplatesRequest createGetAllTemplatesRequest() {
        return new GetAllTemplatesRequest();
    }

    /**
     * Create an instance of {@link GetTemplateByIdResponse }
     * 
     */
    public GetTemplateByIdResponse createGetTemplateByIdResponse() {
        return new GetTemplateByIdResponse();
    }

    /**
     * Create an instance of {@link Template }
     * 
     */
    public Template createTemplate() {
        return new Template();
    }

    /**
     * Create an instance of {@link DeleteRecipientRequest }
     * 
     */
    public DeleteRecipientRequest createDeleteRecipientRequest() {
        return new DeleteRecipientRequest();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link SendNotificationRequest }
     * 
     */
    public SendNotificationRequest createSendNotificationRequest() {
        return new SendNotificationRequest();
    }

    /**
     * Create an instance of {@link EmailAddress }
     * 
     */
    public EmailAddress createEmailAddress() {
        return new EmailAddress();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link SendNotificationResponse }
     * 
     */
    public SendNotificationResponse createSendNotificationResponse() {
        return new SendNotificationResponse();
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

    /**
     * Create an instance of {@link GetRecipientByIdRequest }
     * 
     */
    public GetRecipientByIdRequest createGetRecipientByIdRequest() {
        return new GetRecipientByIdRequest();
    }

    /**
     * Create an instance of {@link UpdateTemplateRequest }
     * 
     */
    public UpdateTemplateRequest createUpdateTemplateRequest() {
        return new UpdateTemplateRequest();
    }

    /**
     * Create an instance of {@link UpdateTemplateResponse }
     * 
     */
    public UpdateTemplateResponse createUpdateTemplateResponse() {
        return new UpdateTemplateResponse();
    }

    /**
     * Create an instance of {@link GetRecipientsByEmailResponse }
     * 
     */
    public GetRecipientsByEmailResponse createGetRecipientsByEmailResponse() {
        return new GetRecipientsByEmailResponse();
    }

    /**
     * Create an instance of {@link Recipient }
     * 
     */
    public Recipient createRecipient() {
        return new Recipient();
    }

    /**
     * Create an instance of {@link GetTemplatesByNameRequest }
     * 
     */
    public GetTemplatesByNameRequest createGetTemplatesByNameRequest() {
        return new GetTemplatesByNameRequest();
    }

    /**
     * Create an instance of {@link GetAllNotificationsByRecipientIdResponse }
     * 
     */
    public GetAllNotificationsByRecipientIdResponse createGetAllNotificationsByRecipientIdResponse() {
        return new GetAllNotificationsByRecipientIdResponse();
    }

    /**
     * Create an instance of {@link UpdateRecipientRequest }
     * 
     */
    public UpdateRecipientRequest createUpdateRecipientRequest() {
        return new UpdateRecipientRequest();
    }

    /**
     * Create an instance of {@link RecipientParameter }
     * 
     */
    public RecipientParameter createRecipientParameter() {
        return new RecipientParameter();
    }

    /**
     * Create an instance of {@link UpdateUserRequest }
     * 
     */
    public UpdateUserRequest createUpdateUserRequest() {
        return new UpdateUserRequest();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link CreateTemplateResponse }
     * 
     */
    public CreateTemplateResponse createCreateTemplateResponse() {
        return new CreateTemplateResponse();
    }

    /**
     * Create an instance of {@link GetAllNotificationsByRecipientIdRequest }
     * 
     */
    public GetAllNotificationsByRecipientIdRequest createGetAllNotificationsByRecipientIdRequest() {
        return new GetAllNotificationsByRecipientIdRequest();
    }

    /**
     * Create an instance of {@link GetAllNotificationsRequest }
     * 
     */
    public GetAllNotificationsRequest createGetAllNotificationsRequest() {
        return new GetAllNotificationsRequest();
    }

    /**
     * Create an instance of {@link GetUserRequest }
     * 
     */
    public GetUserRequest createGetUserRequest() {
        return new GetUserRequest();
    }

    /**
     * Create an instance of {@link CreateRecipientResponse }
     * 
     */
    public CreateRecipientResponse createCreateRecipientResponse() {
        return new CreateRecipientResponse();
    }

    /**
     * Create an instance of {@link GetAllRecipientsResponse }
     * 
     */
    public GetAllRecipientsResponse createGetAllRecipientsResponse() {
        return new GetAllRecipientsResponse();
    }

    /**
     * Create an instance of {@link DeleteTemplateRequest }
     * 
     */
    public DeleteTemplateRequest createDeleteTemplateRequest() {
        return new DeleteTemplateRequest();
    }

    /**
     * Create an instance of {@link GetAllTemplatesResponse }
     * 
     */
    public GetAllTemplatesResponse createGetAllTemplatesResponse() {
        return new GetAllTemplatesResponse();
    }

    /**
     * Create an instance of {@link GetAllNotificationsResponse }
     * 
     */
    public GetAllNotificationsResponse createGetAllNotificationsResponse() {
        return new GetAllNotificationsResponse();
    }

    /**
     * Create an instance of {@link CreateTemplateRequest }
     * 
     */
    public CreateTemplateRequest createCreateTemplateRequest() {
        return new CreateTemplateRequest();
    }

    /**
     * Create an instance of {@link GetRecipientsByEmailRequest }
     * 
     */
    public GetRecipientsByEmailRequest createGetRecipientsByEmailRequest() {
        return new GetRecipientsByEmailRequest();
    }

    /**
     * Create an instance of {@link GetTemplateByIdRequest }
     * 
     */
    public GetTemplateByIdRequest createGetTemplateByIdRequest() {
        return new GetTemplateByIdRequest();
    }

    /**
     * Create an instance of {@link GetAllRecipientsRequest }
     * 
     */
    public GetAllRecipientsRequest createGetAllRecipientsRequest() {
        return new GetAllRecipientsRequest();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserRequest }
     * 
     */
    public CreateUserRequest createCreateUserRequest() {
        return new CreateUserRequest();
    }

    /**
     * Create an instance of {@link DeleteUserRequest }
     * 
     */
    public DeleteUserRequest createDeleteUserRequest() {
        return new DeleteUserRequest();
    }

    /**
     * Create an instance of {@link GetNotificationByIdRequest }
     * 
     */
    public GetNotificationByIdRequest createGetNotificationByIdRequest() {
        return new GetNotificationByIdRequest();
    }

    /**
     * Create an instance of {@link GetRecipientByIdResponse }
     * 
     */
    public GetRecipientByIdResponse createGetRecipientByIdResponse() {
        return new GetRecipientByIdResponse();
    }

    /**
     * Create an instance of {@link GetTemplatesByNameResponse }
     * 
     */
    public GetTemplatesByNameResponse createGetTemplatesByNameResponse() {
        return new GetTemplatesByNameResponse();
    }

    /**
     * Create an instance of {@link DeleteTemplateResponse }
     * 
     */
    public DeleteTemplateResponse createDeleteTemplateResponse() {
        return new DeleteTemplateResponse();
    }

    /**
     * Create an instance of {@link UpdateRecipientResponse }
     * 
     */
    public UpdateRecipientResponse createUpdateRecipientResponse() {
        return new UpdateRecipientResponse();
    }

    /**
     * Create an instance of {@link UpdateUserRoleRequest }
     * 
     */
    public UpdateUserRoleRequest createUpdateUserRoleRequest() {
        return new UpdateUserRoleRequest();
    }

    /**
     * Create an instance of {@link CreateRecipientRequest }
     * 
     */
    public CreateRecipientRequest createCreateRecipientRequest() {
        return new CreateRecipientRequest();
    }

    /**
     * Create an instance of {@link DeleteRecipientResponse }
     * 
     */
    public DeleteRecipientResponse createDeleteRecipientResponse() {
        return new DeleteRecipientResponse();
    }

    /**
     * Create an instance of {@link GetNotificationByIdResponse }
     * 
     */
    public GetNotificationByIdResponse createGetNotificationByIdResponse() {
        return new GetNotificationByIdResponse();
    }

}
