package pl.rashid.siwz.server;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.rashid.siwz.*;
import pl.rashid.siwz.Template;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Endpoint
public class TemplateEndpoint {
    private static final String NAMESPACE_URI = "http://rashid.pl/siwz";

    private TemplateRepository templateRepository;

    @Autowired
    public TemplateEndpoint(TemplateRepository templateRepository) {
        this.templateRepository = templateRepository;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTemplateByIdRequest")
    @ResponsePayload
    public GetTemplateByIdResponse getTemplateById(@RequestPayload GetTemplateByIdRequest request)
            throws ResourceNotFoundException{

        GetTemplateByIdResponse response = new GetTemplateByIdResponse();

        Template template = templateRepository.findById(Principals.getPrincipal(), request.getId());

        if(template == null)
            throw new ResourceNotFoundException();

        template.setContent(template.getContent());
        response.setTemplate(template);

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTemplatesByNameRequest")
    @ResponsePayload
    public GetTemplatesByNameResponse getTemplatesByName(@RequestPayload GetTemplatesByNameRequest request) {

        GetTemplatesByNameResponse response = new GetTemplatesByNameResponse();
        response.getTemplate().addAll(templateRepository.findAllByName(Principals.getPrincipal(), request.getName()));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllTemplatesRequest")
    @ResponsePayload
    public GetAllTemplatesResponse getAllTemplates(@RequestPayload GetAllTemplatesRequest request) {

        GetAllTemplatesResponse response = new GetAllTemplatesResponse();
        response.getTemplate().addAll(templateRepository.findAll(Principals.getPrincipal()));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createTemplateRequest")
    @ResponsePayload
    public CreateTemplateResponse createTemplate(@RequestPayload CreateTemplateRequest request)
            throws DatatypeConfigurationException {

        CreateTemplateResponse response = new CreateTemplateResponse();

        Template template = new Template();
        template.setName(request.getName());
        template.setSubject(request.getSubject());
        template.setContent(request.getContent());
        template.setCreationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));
        template.setLastModificationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));


        response.setTemplate(templateRepository.save(Principals.getPrincipal(), template));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateTemplateRequest")
    @ResponsePayload
    public UpdateTemplateResponse updateTemplate(@RequestPayload UpdateTemplateRequest request)
            throws ResourceNotFoundException, DatatypeConfigurationException {

        UpdateTemplateResponse response = new UpdateTemplateResponse();

        Template template = templateRepository.findById(Principals.getPrincipal(), request.getId());

        if(template == null)
            throw new ResourceNotFoundException();

        template.setName(request.getName());
        template.setSubject(request.getSubject());
        template.setContent(request.getContent());
        template.setLastModificationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));

        response.setTemplate(templateRepository.save(Principals.getPrincipal(), template));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteTemplateRequest")
    @ResponsePayload
    public DeleteTemplateResponse deleteTemplate(@RequestPayload DeleteTemplateRequest request)
            throws ResourceNotFoundException{

        DeleteTemplateResponse response = new DeleteTemplateResponse();

        Template template = templateRepository.findById(Principals.getPrincipal(), request.getId());

        if(template == null)
            throw new ResourceNotFoundException();

        templateRepository.remove(template);

        response.setSuccessful(true);

        return response;
    }
}
