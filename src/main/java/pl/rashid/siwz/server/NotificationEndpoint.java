package pl.rashid.siwz.server;

import com.samskivert.mustache.*;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.rashid.siwz.*;
import pl.rashid.siwz.Template;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Endpoint
public class NotificationEndpoint {
    private static final String NAMESPACE_URI = "http://rashid.pl/siwz";

    private NotificationRepository notificationRepository;
    private UserRepository userRepository;
    private RecipientRepository recipientRepository;
    private TemplateRepository templateRepository;

    @Autowired
    public NotificationEndpoint(NotificationRepository notificationRepository,
                                UserRepository userRepository,
                                RecipientRepository recipientRepository,
                                TemplateRepository templateRepository) {

        this.notificationRepository = notificationRepository;
        this.userRepository = userRepository;
        this.recipientRepository = recipientRepository;
        this.templateRepository = templateRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllNotificationsByRecipientIdRequest")
    @ResponsePayload
    public GetAllNotificationsByRecipientIdResponse getAllNotificationsByRecipientId(
            @RequestPayload GetAllNotificationsByRecipientIdRequest request) {

        GetAllNotificationsByRecipientIdResponse response = new GetAllNotificationsByRecipientIdResponse();

        response.getNotification().addAll(notificationRepository.findAllByRecipientId(Principals.getPrincipal(), request.getRecipientId()));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllNotificationsRequest")
    @ResponsePayload
    public GetAllNotificationsResponse getAllNotifications(@RequestPayload GetAllNotificationsRequest request) {

        GetAllNotificationsResponse response = new GetAllNotificationsResponse();
        response.getNotification().addAll(notificationRepository.findAll(Principals.getPrincipal()));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNotificationByIdRequest")
    @ResponsePayload
    public GetNotificationByIdResponse getNotificationById(@RequestPayload GetNotificationByIdRequest request)
            throws ResourceNotFoundException {

        GetNotificationByIdResponse response = new GetNotificationByIdResponse();

        Notification notification = notificationRepository.findById(Principals.getPrincipal(), request.getId());

        if(notification == null)
            throw new ResourceNotFoundException();

        response.setNotification(notification);

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "sendNotificationRequest")
    @ResponsePayload
    public SendNotificationResponse sendNotification(@RequestPayload SendNotificationRequest request)
            throws DatatypeConfigurationException, ResourceNotFoundException, UnsupportedEncodingException,
            AddressException {




        // Checking existance and permissions of recipient and template
        Recipient recipient = recipientRepository.findById(Principals.getPrincipal(), request.getRecipientId());
        if(recipient == null)
            throw new ResourceNotFoundException("Recipient not found");


        Template template = templateRepository.findById(Principals.getPrincipal(), request.getTemplateId());
        if(template == null)
            throw new ResourceNotFoundException("Template not found");





        // Trying to bind variables with template
        Boolean skipEmptyValues = (request.isSkipEmptyVariables() == null) ? false : request.isSkipEmptyVariables();

        HashMap<String, String> variables = new HashMap<String, String>();
        variables.put("email", recipient.getEmail());
        for(RecipientParameter parameter : recipient.getParameter()) {
            variables.put(parameter.getName(), parameter.getValue());
        }

        com.samskivert.mustache.Template contentTemplate;
        com.samskivert.mustache.Template subjectTemplate;

        if(skipEmptyValues) {
            contentTemplate = Mustache.compiler().defaultValue("").compile(template.getContent());
            subjectTemplate = Mustache.compiler().defaultValue("").compile(template.getSubject());
        } else {
            contentTemplate = Mustache.compiler().compile(template.getContent());
            subjectTemplate = Mustache.compiler().compile(template.getSubject());
        }

        String content = contentTemplate.execute(variables);
        String subject = subjectTemplate.execute(variables);




        // Fetching logged User
        User user = userRepository.findByUsername(Principals.getName());



        // Creating notification entry
        Notification notification = new Notification();
        notification.setRecipientId(request.getRecipientId());
        notification.setRecipient(recipient);
        notification.setTemplateId(request.getTemplateId());
        notification.setTemplate(template);
        notification.setSkipEmptyVariables(skipEmptyValues);

        notification.setFromAddress(
                (request.getFromAddress() == null) ? user.getDefaultFromAddress() : request.getFromAddress());
        notification.setReplyToAddress(
                (request.getReplyToAddress() == null) ? user.getDefaultReplyToAddress() : request.getReplyToAddress());


        new InternetAddress(
                notification.getFromAddress().getEmail(),
                notification.getFromAddress().getDisplayName()).
                validate();

        new InternetAddress(
                notification.getReplyToAddress().getEmail(),
                notification.getReplyToAddress().getDisplayName()).
                validate();


        notification.setCreationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));


        SendNotificationResponse response = new SendNotificationResponse();
        response.setNotification(notificationRepository.save(Principals.getPrincipal(), notification));



        // Start sending thread
        NotificationSendingThread notificationSendingThread = new NotificationSendingThread(notification, content, subject);
        notificationSendingThread.start();

        return response;
    }
}
