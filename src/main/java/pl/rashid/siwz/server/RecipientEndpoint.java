package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.rashid.siwz.*;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Date;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Endpoint
public class RecipientEndpoint {
    private static final String NAMESPACE_URI = "http://rashid.pl/siwz";

    private RecipientRepository recipientRepository;

    @Autowired
    public RecipientEndpoint(RecipientRepository recipientRepository) {
        this.recipientRepository = recipientRepository;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllRecipientsRequest")
    @ResponsePayload
    public GetAllRecipientsResponse getAllRecipients(@RequestPayload GetAllRecipientsRequest request) {

        GetAllRecipientsResponse response = new GetAllRecipientsResponse();

        response.getRecipient().addAll(recipientRepository.findAll(Principals.getPrincipal()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRecipientByIdRequest")
    @ResponsePayload
    public GetRecipientByIdResponse getRecipientById(@RequestPayload GetRecipientByIdRequest request) {

        GetRecipientByIdResponse response = new GetRecipientByIdResponse();

        response.setRecipient(recipientRepository.findById(Principals.getPrincipal(), request.getId()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRecipientByEmailRequest")
    @ResponsePayload
    public GetRecipientsByEmailResponse getRecipientsByEmail(@RequestPayload GetRecipientsByEmailRequest request) {

        GetRecipientsByEmailResponse response = new GetRecipientsByEmailResponse();

        response.getRecipient().addAll(recipientRepository.findAllByEmail(Principals.getPrincipal(), request.getEmail()));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createRecipientRequest")
    @ResponsePayload
    public CreateRecipientResponse createRecipient(@RequestPayload CreateRecipientRequest request)
            throws AddressException {


        new InternetAddress(request.getEmail()).
                validate();


        CreateRecipientResponse response = new CreateRecipientResponse();

        Recipient recipient = new Recipient();
        recipient.setEmail(request.getEmail());
        recipient.getParameter().addAll(request.getParameter());

        response.setRecipient(recipientRepository.save(Principals.getPrincipal(), recipient));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateRecipientRequest")
    @ResponsePayload
    public UpdateRecipientResponse updateRecipient(@RequestPayload UpdateRecipientRequest request)
            throws ResourceNotFoundException, AddressException {

        new InternetAddress(request.getEmail()).
                validate();

        UpdateRecipientResponse response = new UpdateRecipientResponse();

        Recipient recipient = recipientRepository.findById(Principals.getPrincipal(), request.getId());

        if(recipient == null)
            throw new ResourceNotFoundException();

        recipient.setEmail(request.getEmail());
        recipient.getParameter().addAll(request.getParameter());

        response.setRecipient(recipientRepository.save(Principals.getPrincipal(), recipient));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteRecipientRequest")
    @ResponsePayload
    public DeleteRecipientResponse deleteRecipient(@RequestPayload DeleteRecipientRequest request)
            throws ResourceNotFoundException{

        DeleteRecipientResponse response = new DeleteRecipientResponse();

        Recipient recipient = recipientRepository.findById(Principals.getPrincipal(), request.getId());

        if(recipient == null)
            throw new ResourceNotFoundException();

        recipientRepository.remove(recipient);

        response.setSuccessful(true);

        return response;
    }
}
