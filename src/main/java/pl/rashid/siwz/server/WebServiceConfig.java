package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.soap.security.wss4j.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.security.wss4j.callback.SpringSecurityPasswordValidationCallbackHandler;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.util.List;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Autowired
    private UserRepository userRepository;


    @Bean
    public SpringSecurityPasswordValidationCallbackHandler securityCallbackHandler(){
        SpringSecurityPasswordValidationCallbackHandler callbackHandler = new SpringSecurityPasswordValidationCallbackHandler();
        callbackHandler.setUserDetailsService(new UserDetailsService(userRepository));
        return callbackHandler;
    }

    @Bean
    public Wss4jSecurityInterceptor securityInterceptor(){
        Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
        securityInterceptor.setValidationActions("Timestamp UsernameToken");
        securityInterceptor.setValidationCallbackHandler(securityCallbackHandler());
        return securityInterceptor;
    }

    @Override
    public void addInterceptors(List interceptors) {
        interceptors.add(securityInterceptor());
    }

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);

        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "templates")
    public DefaultWsdl11Definition templatesWsdl11Definition(XsdSchema templatesSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("TemplatesPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://rashid.pl/siwz");
        wsdl11Definition.setSchema(templatesSchema);

        return wsdl11Definition;
    }

    @Bean(name = "users")
    public DefaultWsdl11Definition usersWsdl11Definition(XsdSchema usersSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("UsersPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://rashid.pl/siwz");
        wsdl11Definition.setSchema(usersSchema);

        return wsdl11Definition;
    }

    @Bean(name = "recipients")
    public DefaultWsdl11Definition recipientsWsdl11Definition(XsdSchema recipientsSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("RecipientsPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://rashid.pl/siwz");
        wsdl11Definition.setSchema(recipientsSchema);

        return wsdl11Definition;
    }

    @Bean(name = "notifications")
    public DefaultWsdl11Definition notificationsWsdl11Definition(XsdSchema notificationsSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("NotificationsPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://rashid.pl/siwz");
        wsdl11Definition.setSchema(notificationsSchema);

        return wsdl11Definition;
    }

    @Bean
    public SimpleXsdSchema models() {
        return new SimpleXsdSchema(new ClassPathResource("models.xsd"));
    }

    @Bean
    public XsdSchema templatesSchema() {
        return new SimpleXsdSchema(new ClassPathResource("templates.xsd"));
    }

    @Bean
    public XsdSchema usersSchema() {
        return new SimpleXsdSchema(new ClassPathResource("users.xsd"));
    }

    @Bean
    public XsdSchema recipientsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("recipients.xsd"));
    }

    @Bean
    public XsdSchema notificationsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("notifications.xsd"));
    }
}
