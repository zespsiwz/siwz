package pl.rashid.siwz.server;

/**
 * Created by Dawid Rashid on 2016-06-26.
 */
public class PermissionDeniedException extends Exception {

    public PermissionDeniedException() {
        super("Permission denied");
    }

    public PermissionDeniedException(String msg) {
        super(msg);
    }
} 
