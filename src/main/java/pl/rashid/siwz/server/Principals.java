package pl.rashid.siwz.server;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;
import java.util.List;

/**
 * Created by Dawid Rashid on 2016-06-26.
 */
public class Principals {

    public static Principal getPrincipal() {
        return (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static String getName() {
        return getPrincipal().getName();
    }

    public static boolean hasRole(String role) {

        for (GrantedAuthority auth : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (role.equals(auth.getAuthority()))
                return true;
        }

        return false;
    }
}
