package pl.rashid.siwz.server;

/**
 * Created by Dawid Rashid on 2016-06-26.
 */
public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException() {
        super("Resource not found");
    }

    public ResourceNotFoundException(String msg) {
        super(msg);
    }
}
