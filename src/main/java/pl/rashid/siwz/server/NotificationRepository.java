package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import pl.rashid.siwz.Notification;
import pl.rashid.siwz.Template;

import java.security.Principal;
import java.util.List;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Component
public class NotificationRepository {

    @Autowired
    MongoOperations mongo;

    private Query getQuery(Principal principal) {
        Assert.notNull(principal);

        Query query = new Query();
        query.addCriteria(Criteria.where("ownerUsername").is(principal.getName()));

        return query;
    }

    public List<Notification> findAll(Principal principal) {

        return mongo.find(getQuery(principal), Notification.class);
    }

    public List<Notification> findAllByRecipientId(Principal principal, String recipientId) {
        Assert.notNull(recipientId);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("recipientId").is(recipientId));

        return mongo.find(query, Notification.class);
    }

    public Notification findById(Principal principal, String id) {
        Assert.notNull(id);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("id").is(id));

        return mongo.findOne(query, Notification.class);
    }

    public Notification save(Principal principal, Notification notification) {
        Assert.notNull(principal);
        Assert.notNull(notification);

        notification.setOwnerUsername(principal.getName());
        mongo.save(notification);

        return notification;
    }

    public void remove(Template template) {
        Assert.notNull(template);

        mongo.remove(template);
    }
}
