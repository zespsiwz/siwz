package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import pl.rashid.siwz.Recipient;

import java.security.Principal;
import java.util.List;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Component
public class RecipientRepository {

    @Autowired
    MongoOperations mongo;

    private Query getQuery(Principal principal) {
        Assert.notNull(principal);

        Query query = new Query();
        query.addCriteria(Criteria.where("ownerUsername").is(principal.getName()));

        return query;
    }

    public List<Recipient> findAll(Principal principal) {

        return mongo.find(getQuery(principal), Recipient.class);
    }

    public List<Recipient> findAllByEmail(Principal principal, String email) {
        Assert.notNull(email);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("email").is(email));

        return mongo.find(query, Recipient.class);
    }

    public Recipient findById(Principal principal, String id) {
        Assert.notNull(id);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("id").is(id));

        return mongo.findOne(query, Recipient.class);
    }

    public Recipient save(Principal principal, Recipient recipient) {
        Assert.notNull(principal);
        Assert.notNull(recipient);

        recipient.setOwnerUsername(principal.getName());
        mongo.save(recipient);

        return recipient;
    }

    public void remove(Recipient recipient) {
        Assert.notNull(recipient);

        mongo.remove(recipient);
    }
}
