package pl.rashid.siwz.server;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.rashid.siwz.*;
import pl.rashid.siwz.helpers.StringHelper;
import pl.rashid.siwz.User;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Endpoint
public class UserEndpoint {
    private static final String NAMESPACE_URI = "http://rashid.pl/siwz";

    private UserRepository userRepository;

    @Autowired
    public UserEndpoint(UserRepository userRepository) {
        this.userRepository = userRepository;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserRequest")
    @ResponsePayload
    public GetUserResponse getUser(@RequestPayload GetUserRequest request) {

        GetUserResponse response = new GetUserResponse();

        User user = userRepository.findByUsername(Principals.getPrincipal().getName());
        user.setPassword(null);

        response.setUser(user);

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createUserRequest")
    @ResponsePayload
    public CreateUserResponse createUser(@RequestPayload CreateUserRequest request)
            throws PermissionDeniedException, ResourceAlreadyExistsException, DatatypeConfigurationException,
            UnsupportedEncodingException, AddressException {

        CreateUserResponse response = new CreateUserResponse();

        if(!Principals.hasRole("ROLE_ADMIN"))
            throw new PermissionDeniedException();

        User user = userRepository.findByUsername(request.getUsername());

        if(user != null)
            throw new ResourceAlreadyExistsException();

        new InternetAddress(
                request.getDefaultFromAddress().getEmail(),
                request.getDefaultFromAddress().getDisplayName()).
                validate();

        new InternetAddress(
                request.getDefaultReplyToAddress().getEmail(),
                request.getDefaultReplyToAddress().getDisplayName()).
                validate();

        user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(StringHelper.generateRandom(32));
        user.setRole(request.getRole());
        user.setDefaultFromAddress(request.getDefaultFromAddress());
        user.setDefaultReplyToAddress(request.getDefaultReplyToAddress());
        user.setCreationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));
        user.setLastModificationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));

        userRepository.save(user);

        response.setUser(user);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateUserRequest")
    @ResponsePayload
    public UpdateUserResponse updateUser(@RequestPayload UpdateUserRequest request)
            throws DatatypeConfigurationException, AddressException, UnsupportedEncodingException {

        UpdateUserResponse response = new UpdateUserResponse();

        User user = userRepository.findByUsername(Principals.getPrincipal().getName());

        new InternetAddress(
                    request.getDefaultFromAddress().getEmail(),
                    request.getDefaultFromAddress().getDisplayName()).
                    validate();

        new InternetAddress(
                    request.getDefaultReplyToAddress().getEmail(),
                    request.getDefaultReplyToAddress().getDisplayName()).
                    validate();

        user.setDefaultFromAddress(request.getDefaultFromAddress());
        user.setDefaultReplyToAddress(request.getDefaultReplyToAddress());
        user.setLastModificationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));

        userRepository.save(user);

        response.setUser(user);

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateUserRoleRequest")
    @ResponsePayload
    public UpdateUserResponse updateUserRole(@RequestPayload UpdateUserRoleRequest request)
            throws PermissionDeniedException, ResourceNotFoundException, DatatypeConfigurationException {

        UpdateUserResponse response = new UpdateUserResponse();

        if(!Principals.hasRole("ROLE_ADMIN"))
            throw new PermissionDeniedException();

        User user = userRepository.findById(request.getId());

        if(user == null)
            throw new ResourceNotFoundException();

        user.setRole(request.getRole());
        user.setLastModificationDate(
                DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar()));

        userRepository.save(user);

        response.setUser(user);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteUserRequest")
    @ResponsePayload
    public DeleteUserResponse deleteUser(@RequestPayload DeleteUserRequest request)
            throws PermissionDeniedException, ResourceNotFoundException{

        DeleteUserResponse response = new DeleteUserResponse();

        User user = userRepository.findById(request.getId());

        if(!Principals.hasRole("ROLE_ADMIN") ||
                (user != null && !user.getUsername().equals(Principals.getName())))
            throw new PermissionDeniedException();

        if(user == null)
            throw new ResourceNotFoundException();

        userRepository.remove(user);

        response.setSuccessful(true);

        return response;
    }
}
