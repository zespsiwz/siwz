package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import pl.rashid.siwz.Template;

import java.security.Principal;
import java.util.List;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Component
public class TemplateRepository {

    @Autowired
    MongoOperations mongo;

    private Query getQuery(Principal principal) {
        Assert.notNull(principal);

        Query query = new Query();
        query.addCriteria(Criteria.where("ownerUsername").is(principal.getName()));

        return query;
    }

    public List<Template> findAll(Principal principal) {

        return mongo.find(getQuery(principal), Template.class);
    }

    public List<Template> findAllByName(Principal principal, String name) {
        Assert.notNull(name);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("name").is(name));

        return mongo.find(query, Template.class);
    }

    public Template findById(Principal principal, String id) {
        Assert.notNull(id);

        Query query = getQuery(principal);
        query.addCriteria(Criteria.where("id").is(id));

        return mongo.findOne(query, Template.class);
    }

    public Template save(Principal principal, Template template) {
        Assert.notNull(principal);
        Assert.notNull(template);

        template.setOwnerUsername(principal.getName());
        mongo.save(template);

        return template;
    }

    public void remove(Template template) {
        Assert.notNull(template);

        mongo.remove(template);
    }
}
