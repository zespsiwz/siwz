package pl.rashid.siwz.server;

import org.jsoup.Jsoup;
import pl.rashid.siwz.Notification;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by Dawid Rashid on 2016-07-02.
 */
public class NotificationSendingThread extends Thread {

    private static final String ENCODING = "utf-8";
    private Notification notification;
    private String content;
    private String subject;

    public NotificationSendingThread(Notification notification, String content, String subject) {
        this.notification = notification;
        this.content = content;
        this.subject = subject;
    }

    @Override
    public void run() {

        System.out.println("Sending email");

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("dawidrashidsiwz@gmail.com","9v57sC5qS3R3S5Xa");
                    }
                });

        try {

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(
                    notification.getFromAddress().getEmail(),
                    notification.getFromAddress().getDisplayName()));


            message.setReplyTo(new Address[] {new InternetAddress(
                    notification.getReplyToAddress().getEmail(),
                    notification.getReplyToAddress().getDisplayName())});


            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(notification.getRecipient().getEmail()));

            message.setSubject(subject);



            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setText(Jsoup.parse(content).text(), ENCODING);

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(content, "text/html; charset=" + ENCODING);

            Multipart multiPart = new MimeMultipart("alternative");
            multiPart.addBodyPart(textPart);
            multiPart.addBodyPart(htmlPart);
            message.setContent(multiPart);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
