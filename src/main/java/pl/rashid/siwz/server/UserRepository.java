package pl.rashid.siwz.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import pl.rashid.siwz.User;

/**
 * Created by Dawid Rashid on 2016-06-17.
 */
@Component
public class UserRepository {

    @Autowired
    MongoOperations mongo;

    public User findByUsername(String username) {
        Assert.notNull(username);

        Query query = new Query();
        query.addCriteria(Criteria.where("username").is(username));

        return mongo.findOne(query, User.class);
    }

    public User findById(String id) {
        Assert.notNull(id);

        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));

        return mongo.findOne(query, User.class);
    }

    public User save(User user) {
        Assert.notNull(user);

        mongo.save(user);

        return user;
    }

    public void remove(User user) {
        Assert.notNull(user);

        mongo.remove(user);
    }
}
