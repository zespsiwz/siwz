package pl.rashid.siwz.server;

/**
 * Created by Dawid Rashid on 2016-06-26.
 */
public class ResourceAlreadyExistsException extends Exception {

    public ResourceAlreadyExistsException() {
        super("Resource already exists");
    }

    public ResourceAlreadyExistsException(String msg) {
        super(msg);
    }
}
