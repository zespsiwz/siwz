package pl.rashid.siwz.server;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.core.userdetails.User;

import java.util.HashSet;

/**
 * Created by Dawid Rashid on 2016-06-25.
 */
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private UserRepository userRepository;

    public UserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException
    {
        pl.rashid.siwz.User u = userRepository.findByUsername(username);
        if(u == null)
            throw new UsernameNotFoundException("User not found");

        HashSet authorities = new HashSet<GrantedAuthority>(1);
        authorities.add(new SimpleGrantedAuthority(u.getRole().value()));
        UserDetails user = new User(u.getUsername(), u.getPassword(), true, true, true, true, authorities);
        return user;
    }
}
