package pl.rashid.siwz.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SIwZApplication {

	public static void main(String[] args) {
		SpringApplication.run(SIwZApplication.class, args);
	}
}
